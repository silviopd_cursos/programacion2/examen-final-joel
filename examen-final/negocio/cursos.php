<?php

require_once '../datos/Conexion.clase.php';

class cursos extends Conexion {

    private $id_cursos, $titulo;

    function getId_cursos()
    {
        return $this->id_cursos;
    }

    function getTitulo()
    {
        return $this->titulo;
    }

    function setId_cursos($id_cursos)
    {
        $this->id_cursos = $id_cursos;
    }

    function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    public function agregar_cursos()
    {
        $this->dblink->beginTransaction();

        try {

            $sql = "CALL registrar_cursos(:p_titulo)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_titulo", $this->getTitulo());
            $sentencia->execute();

            $this->dblink->commit();

            return true;
        } catch (Exception $exc) {
            $this->dblink->rollBack();
            throw $exc;
        }

        return false;
    }
    
    public function listar()
    {
        try {
            $sql = "select * from cursos";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }



}
