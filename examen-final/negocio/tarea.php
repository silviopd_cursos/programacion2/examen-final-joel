<?php

require_once '../datos/Conexion.clase.php';
class tarea extends Conexion {
    
    private $id_tarea, $titulo, $descripcion, $fecha_inicio, $fecha_fin, $archivo_tarea, $id_cursos;
    
    function getId_tarea()
    {
        return $this->id_tarea;
    }

    function getTitulo()
    {
        return $this->titulo;
    }

    function getDescripcion()
    {
        return $this->descripcion;
    }

    function getFecha_inicio()
    {
        return $this->fecha_inicio;
    }

    function getFecha_fin()
    {
        return $this->fecha_fin;
    }

    function getArchivo_tarea()
    {
        return $this->archivo_tarea;
    }

    function getId_cursos()
    {
        return $this->id_cursos;
    }

    function setId_tarea($id_tarea)
    {
        $this->id_tarea = $id_tarea;
    }

    function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    function setFecha_inicio($fecha_inicio)
    {
        $this->fecha_inicio = $fecha_inicio;
    }

    function setFecha_fin($fecha_fin)
    {
        $this->fecha_fin = $fecha_fin;
    }

    function setArchivo_tarea($archivo_tarea)
    {
        $this->archivo_tarea = $archivo_tarea;
    }

    function setId_cursos($id_cursos)
    {
        $this->id_cursos = $id_cursos;
    }

        
    public function agregar_tarea_cursos()
    {
        $this->dblink->beginTransaction();

        try {

            $sql = "CALL registrar_tarea_cursos( :p_titulo, :p_descripcion, :p_fecha_inicio, :p_fecha_fin, :p_archivo_tarea,:p_id_cursos);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_titulo", $this->getTitulo());
            $sentencia->bindValue(":p_descripcion", $this->getDescripcion());
            $sentencia->bindValue(":p_fecha_inicio", $this->getFecha_inicio());
            $sentencia->bindValue(":p_fecha_fin", $this->getFecha_fin());
            $sentencia->bindValue(":p_archivo_tarea", $this->getArchivo_tarea());
            $sentencia->bindValue(":p_id_cursos", $this->getId_cursos());
            $sentencia->execute();

            $this->dblink->commit();

            return true;
        } catch (Exception $exc) {
            $this->dblink->rollBack();
            throw $exc;
        }

        return false;
    }
    
    public function listar()
    {
        try {
            $sql = "select t.*,c.nombre_cursos from tarea t inner join cursos c on t.id_cursos=c.id_cursos";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function buscar_listar()
    {
        try {
            $sql = "select t.*,c.nombre_cursos from tarea t inner join cursos c on t.id_cursos=c.id_cursos where c.id_cursos = :p_id_cursos";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_cursos", $this->getId_cursos());
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
     public function leerDatos()
    {
        try {
            $sql = "select * from tarea where id_tarea = :p_id_tarea";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_tarea", $this->getId_tarea());
            $sentencia->execute();
            $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function editar_tarea_cursos()
    {
        $this->dblink->beginTransaction();

        try {

            $sql = "CALL editar_tarea_cursos( :p_id_tarea,:p_titulo, :p_descripcion, :p_fecha_inicio, :p_fecha_fin, :p_archivo_tarea,:p_id_cursos);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_tarea", $this->getId_tarea());
            $sentencia->bindValue(":p_titulo", $this->getTitulo());
            $sentencia->bindValue(":p_descripcion", $this->getDescripcion());
            $sentencia->bindValue(":p_fecha_inicio", $this->getFecha_inicio());
            $sentencia->bindValue(":p_fecha_fin", $this->getFecha_fin());
            $sentencia->bindValue(":p_archivo_tarea", $this->getArchivo_tarea());
            $sentencia->bindValue(":p_id_cursos", $this->getId_cursos());
            $sentencia->execute();

            $this->dblink->commit();

            return true;
        } catch (Exception $exc) {
            $this->dblink->rollBack();
            throw $exc;
        }

        return false;
    }

}
