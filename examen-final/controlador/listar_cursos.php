<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/cursos.php';
require_once '../util/Funciones.php';


try {
    $obj = new cursos();
    $resultado = $obj->listar();

    $listacity = array();
    for ($i = 0; $i < count($resultado); $i++)
    {

        $datos = array(
            "id_cursos" => $resultado[$i]["id_cursos"],
            "nombre_cursos" => $resultado[$i]["nombre_cursos"]
        );

        $listacity[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listacity);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
