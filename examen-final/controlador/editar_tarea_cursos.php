<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/tarea.php';
require_once '../util/Funciones.php';

$id_tarea = $_POST["id_tarea"];
$titulo = $_POST["titulo"];
$descripcion = $_POST["descripcion"];
$fecha_inicio = $_POST["fecha_inicio"];
$fecha_fin = $_POST["fecha_fin"];
$cursos = $_POST["id_cursos"];

try {
    $obj = new tarea();
    $obj->setId_tarea($id_tarea);
    $obj->setTitulo($titulo);
    $obj->setDescripcion($descripcion);
    $obj->setFecha_fin($fecha_fin);
    $obj->setFecha_inicio($fecha_inicio);
    $obj->setId_cursos($cursos);
    $obj->setArchivo_tarea($_FILES['archivo']['name']);

    $resultado = $obj->editar_tarea_cursos();

    $upload_folder = '../imagenes';
    $nombre_archivo = $_FILES['archivo']['name'];
    $tipo_archivo = $_FILES['archivo']['type'];
    $tamano_archivo = $_FILES['archivo']['size'];
    $tmp_archivo = $_FILES['archivo']['tmp_name'];
    
    $archivador = $upload_folder . '/' . $nombre_archivo;
    
    move_uploaded_file($tmp_archivo, $archivador);
   

    Funciones::imprimeJSON(200, "", $resultado);
} catch (Exception $exc) {
    Funciones::imprimeJSON(500, $exc, "");
}