<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/cursos.php';
require_once '../util/Funciones.php';


$titulo = $_POST["titulo"];

try {
    $obj = new cursos();
    $obj->setTitulo($titulo);

    $resultado = $obj->agregar_cursos();

    Funciones::imprimeJSON(200, "", $resultado);
} catch (Exception $exc) {
    Funciones::imprimeJSON(500, $exc, "");
}