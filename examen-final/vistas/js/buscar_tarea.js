$(document).ready(function () {
    cargarComboCursos(cbocursos)
    listar(0)
});

$("#btnBuscar").click(function () {
    var id_cursos = $("#cbocursos").val()
    listar(id_cursos)
})

function listar(id_cursos) {

    if (id_cursos == 0) {
        $.post("../controlador/listar_tarea.php", function () {
        }).done(function (resultado) {
            var datosJSON = resultado;
            if (datosJSON.estado === 200) {
                var html = "";

                html += '<small>';
                html += '<table id="tabla-listado3" class="table table-bordered table-striped">';
                html += '<thead>';
                html += '<tr style="background-color: #ededed; height:25px;">';
                html += '<th>Tarea</th>';
                html += '<th>Titulo</th>';
                html += '<th>Descripcion</th>';
                html += '<th>Fecha Inicio</th>';
                html += '<th>Fecha fin</th>';
                html += '<th>Curso</th>';
                html += '<th>Nombre Archivo</th>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';

                //Detalle
                $.each(datosJSON.datos, function (i, item) {
                    html += '<tr>';
                    html += '<td align="center">' + item.id_tarea + '</td>';
                    html += '<td>' + item.titulo + '</td>';
                    html += '<td>' + item.descripcion + '</td>';
                    html += '<td>' + item.fecha_inicio + '</td>';
                    html += '<td>' + item.fecha_fin + '</td>';
                    html += '<td>' + item.nombre_cursos + '</td>';
                    html += '<td>' + item.archivo_tarea + '</td>';

                    html += '</tr>';
                });

                html += '</tbody>';
                html += '</table>';
                html += '</small>';

                $("#listado").html(html);
            } else {
                alert("error")
            }
        }).fail(function (error) {
            var datosJSON = $.parseJSON(error.responseText);
            alert("error: "+error)
        })
    } else {
        $.post("../controlador/buscar_listar_tarea.php", {id_cursos: id_cursos}, function () {
        }).done(function (resultado) {
            var datosJSON = resultado;
            if (datosJSON.estado === 200) {
                var html = "";

                html += '<small>';
                html += '<table id="tabla-listado3" class="table table-bordered table-striped">';
                html += '<thead>';
                html += '<tr style="background-color: #ededed; height:25px;">';
                html += '<th>Tarea</th>';
                html += '<th>Titulo</th>';
                html += '<th>Descripcion</th>';
                html += '<th>Fecha Inicio</th>';
                html += '<th>Fecha fin</th>';
                html += '<th>Curso</th>';
                html += '<th>Nombre Archivo</th>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';

                //Detalle
                $.each(datosJSON.datos, function (i, item) {
                    html += '<tr>';
                    html += '<td align="center">' + item.id_tarea + '</td>';
                    html += '<td>' + item.titulo + '</td>';
                    html += '<td>' + item.descripcion + '</td>';
                    html += '<td>' + item.fecha_inicio + '</td>';
                    html += '<td>' + item.fecha_fin + '</td>';
                    html += '<td>' + item.nombre_cursos + '</td>';
                    html += '<td>' + item.archivo_tarea + '</td>';

                    html += '</tr>';
                });

                html += '</tbody>';
                html += '</table>';
                html += '</small>';

                $("#listado").html(html);
            } else {
                alert("error")
            }
        }).fail(function (error) {
            var datosJSON = $.parseJSON(error.responseText);
            alert("error: "+error)
        })
    }

}
