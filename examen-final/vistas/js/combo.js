function cargarComboCursos(p_nombreCombo) {
    $.post("../controlador/listar_cursos.php", function () {}).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_cursos + '">' + item.nombre_cursos + '</option>';
            });
            $(p_nombreCombo).html(html);
        } else {
            alert("error")
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        alert("error")
    })
}


